Classes stub generades amb wsconsume de jboss EAP 7.2

* ./wsconsume.sh -k -p es.caib.autenticacio.afirma ~/DSSAfirmaVerifyCertificate.wsdl -w https://afirmapre.caib.es/afirmaws/services/DSSAfirmaVerifyCertificate?wsdl -o ~/
* ./wsconsume.sh -k -n -p es.caib.autenticacio.afirma ~/DSSAfirmaVerifyCertificate.wsdl -o ~/
* ./wsconsume.sh -p es.caib.autenticacio.afirma ~/DSSAfirmaVerifyCertificate.wsdl -o ~/

Ús de keycloak amb docker:

Llistar imatges
`$ docker image list`

Llistar contenidors
`$ docker container list`

Accedir al bash d'un contenidor
`$ docker exec -t -i <containerID> /bin/bash`

Crear una nova imatge a partir d'un fitxer de definició
`$ docker build --tag keycloak/toni . --file keycloak-afirma-docker-composer.dck`

Arrancar keycloak via docker:
`$ docker run -p 8080:8080 -p 9990:9990 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin keycloak/toni:latest`

Afegir mòdul (via jboss-cli.sh)

```
$ ./jboss-cli.sh
[ ] connect
[ ] module add --name=es.caib.autenticacio.afirma --resources=es.caib.afirmaauth.jar --dependencies=org.keycloak.keycloak-core,org.keycloak.keycloak-server-spi,org.keycloak.keycloak-server-spi-private,org.keycloak.keycloak-services,org.jboss.logging,org.jboss.ws.api,org.jboss.resteasy.resteasy-jaxrs,javax.ws.rs.api,javax.jws.api,javax.xml.ws.api,javax.servlet.api
```


Per desplegar un mòdul
`[ ] deployment deploy-file /tmp/es.caib.afirmaauth.jar`


Enllaços que han estat útils en el desenvolupament:
* https://www.baeldung.com/java-keycloak-custom-user-providers
* https://stackoverflow.com/questions/57778240/noclassdeffounderror-in-a-provider-jar-when-using-a-class-from-org-keycloak-auth
* https://github.com/GovernIB/maven
