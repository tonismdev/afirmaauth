/*
 * Copyright 2016 Analytical Graphics, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package es.caib.autenticacio.afirma;
	import java.io.StringReader;
	import java.io.StringWriter;
	import java.security.cert.X509Certificate;
	import java.util.Base64;
    import java.util.Collections;
    import java.util.Enumeration;
    import java.util.HashMap;
    import java.util.Iterator;
	import java.util.LinkedList;
	import java.util.List;
    import java.util.Map;
    import javax.servlet.http.HttpServletRequest;
	import javax.ws.rs.core.HttpHeaders;
	import javax.ws.rs.core.MultivaluedHashMap;
	import javax.ws.rs.core.MultivaluedMap;
	import javax.ws.rs.core.Response;
	import javax.xml.namespace.NamespaceContext;
	import javax.xml.parsers.DocumentBuilder;
	import javax.xml.parsers.DocumentBuilderFactory;
	import javax.xml.transform.Transformer;
	import javax.xml.transform.TransformerFactory;
	import javax.xml.transform.dom.DOMSource;
	import javax.xml.transform.stream.StreamResult;
	import javax.xml.ws.BindingProvider;
    import javax.xml.ws.handler.MessageContext;
    import javax.xml.xpath.XPath;
	import javax.xml.xpath.XPathConstants;
	import javax.xml.xpath.XPathExpression;
	import javax.xml.xpath.XPathFactory;
	import org.jboss.resteasy.spi.HttpRequest;
	import org.jboss.resteasy.spi.ResteasyProviderFactory;
	import org.keycloak.authentication.AuthenticationFlowContext;
	import org.keycloak.authentication.AuthenticationFlowError;
	import org.keycloak.authentication.authenticators.browser.AbstractUsernameFormAuthenticator;
	import org.keycloak.authentication.authenticators.x509.AbstractX509ClientCertificateAuthenticator;
	import org.keycloak.authentication.authenticators.x509.CertificateValidator;
	import org.keycloak.authentication.authenticators.x509.X509AuthenticatorConfigModel;
	import org.keycloak.events.Details;
	import org.keycloak.events.Errors;
	import org.keycloak.forms.login.LoginFormsPages;
	import org.keycloak.forms.login.LoginFormsProvider;
	import org.keycloak.forms.login.freemarker.Templates;
	import org.keycloak.models.ModelDuplicateException;
	import org.keycloak.models.UserModel;
	import org.keycloak.models.utils.FormMessage;
	import org.keycloak.models.utils.KeycloakModelUtils;
	import org.keycloak.representations.idm.OAuth2ErrorRepresentation;
	import org.keycloak.services.ServicesLogger;
	import org.keycloak.utils.MediaType;
	import org.w3c.dom.DOMException;
	import org.w3c.dom.Document;
	import org.w3c.dom.Element;
	import org.w3c.dom.Node;
	import org.w3c.dom.NodeList;
	import org.xml.sax.InputSource;
	import es.caib.autenticacio.afirma.DSSCertificate;
	import es.caib.autenticacio.afirma.DSSCertificateService;

	/**
	 * @author <a href="mailto:asanchezmagraner@dgtic.caib.es">Toni Sanchez Magraner</a>
	 * @author <a href="mailto:pnalyvayko@agi.com">Peter Nalyvayko</a>
	 * @version $Revision: 1 $
	 *
	 */
	public class AfirmaClientCertificateAuthenticator extends AbstractX509ClientCertificateAuthenticator{
		
	    @Override
	    public void close() {

	    }

	    @Override
	    public void authenticate(AuthenticationFlowContext context) {
	    	logger.warn("PROVA!!!!!!");
	        try {

	            dumpContainerAttributes(context);
	 	       
	            X509Certificate[] certs = getCertificateChain(context);
	            
	            // Si l'usuari no té certificats es bota l'autenticació per certificat.
	            if (certs == null || certs.length == 0) {
	            	
	            	// Primer comprovam que el certificat el proporcioni el proxy (apache)
		            HttpServletRequest httpServletRequest = ResteasyProviderFactory.getContextData(HttpServletRequest.class);

		            Enumeration<String> llistaAtributs = httpServletRequest.getAttributeNames();
		            while (llistaAtributs.hasMoreElements()) {
		            	String atribut = llistaAtributs.nextElement();
		            	logger.info("Atributs de la request: ---> " + atribut);
		            }
		           
		            certs = (X509Certificate[]) httpServletRequest.getAttribute("javax.servlet.request.X509Certificate");
		            logger.debug("Longitud de certs de Apache: " + certs.length);
	            	if (certs == null || certs.length ==0) {
		                // No x509 client cert, fall through and
		                // continue processing the rest of the authentication flow
		                logger.debug("[X509ClientCertificateAuthenticator:authenticate] x509 client certificate is not available for mutual SSL.");
		                context.attempted();
		                return;
	            	}
	            }
	            	            
	            X509AuthenticatorConfigModel config = null;
	            if (context.getAuthenticatorConfig() != null && context.getAuthenticatorConfig().getConfig() != null) {
	                config = new X509AuthenticatorConfigModel(context.getAuthenticatorConfig());
	            }
	            if (config == null) {
	                logger.warn("[X509ClientCertificateAuthenticator:authenticate] x509 Client Certificate Authentication configuration is not available.");
	                context.challenge(createInfoResponse(context, "X509 client authentication has not been configured yet"));
	                context.attempted();
	                return;
	            }
	            
	            
	            String certBase64=Base64.getEncoder().encodeToString(certs[0].getEncoded());
	            
	            System.out.println("GOIB - INFO ===== Certificat en Base64: " + certBase64);
	            
	            //String dssXML="<?xml version=\"1.0\" encoding=\"UTF-8\"?><dss:VerifyRequest Profile=\"urn:afirma:dss:1.0:profile:XSS\" RequestID=\"BCFFEEAFJHAGE0\"xmlns:dss=\"urn:oasis:names:tc:dss:1.0:core:schema\" xmlns:afxp=\"urn:afirma:dss:1.0:profile:XSS:schema\"xmlns:vr=\"urn:oasis:names:tc:dss:1.0:profiles:verificationreport:schema#\"xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"><dss:OptionalInputs><dss:ClaimedIdentity><dss:Name>tester</dss:Name></dss:ClaimedIdentity><afxp:ReturnReadableCertificateInfo/><vr:ReturnVerificationReport><vr:CheckOptions><vr:CheckCertificateStatus>true</vr:CheckCertificateStatus></vr:CheckOptions><vr:ReportOptions><vr:IncludeCertificateValues>true</vr:IncludeCertificateValues><vr:IncludeRevocationValues>true</vr:IncludeRevocationValues><vr:ReportDetailLevel>urn:oasis:names:tc:dss:1.0:reportdetail:allDetails</vr:ReportDetailLevel></vr:ReportOptions></vr:ReturnVerificationReport></dss:OptionalInputs><dss:SignatureObject><dss:Other><ds:X509Data><ds:X509Certificate><![CDATA["+ certBase64 + "]]></ds:X509Certificate></ds:X509Data> </dss:Other></dss:SignatureObject></dss:VerifyRequest>";
	            String dssXML="<?xml version=\"1.0\" encoding=\"UTF-8\"?> <dss:VerifyRequest Profile=\"urn:afirma:dss:1.0:profile:XSS\" RequestID=\"BCFFEEAFJHAGE0\" xmlns:dss=\"urn:oasis:names:tc:dss:1.0:core:schema\" xmlns:afxp=\"urn:afirma:dss:1.0:profile:XSS:schema\" xmlns:vr=\"urn:oasis:names:tc:dss:1.0:profiles:verificationreport:schema#\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"> <dss:OptionalInputs> <dss:ClaimedIdentity> <dss:Name>CAIBPRE.AUTENTICA</dss:Name> </dss:ClaimedIdentity> <afxp:ReturnReadableCertificateInfo/> <vr:ReturnVerificationReport> <vr:CheckOptions> <vr:CheckCertificateStatus>true</vr:CheckCertificateStatus> </vr:CheckOptions> <vr:ReportOptions> <vr:IncludeCertificateValues>true</vr:IncludeCertificateValues> <vr:IncludeRevocationValues>true</vr:IncludeRevocationValues> <vr:ReportDetailLevel>urn:oasis:names:tc:dss:1.0:reportdetail:allDetails</vr:ReportDetailLevel> </vr:ReportOptions> </vr:ReturnVerificationReport> </dss:OptionalInputs> <dss:SignatureObject> <dss:Other> <ds:X509Data> <ds:X509Certificate><![CDATA["+ certBase64 + "]]></ds:X509Certificate> </ds:X509Data> </dss:Other> </dss:SignatureObject> </dss:VerifyRequest> ";
	             
	            // Activació debug de SOAP
	            System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
	            System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
	            System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
	            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
	            // FI Activació debug de SOAP
	            
	            
	            System.out.println("GOIB - INFO ===== Sol·licitud dssX509XMLv5: " + dssXML);
	            // Validam contra @firma
	            logger.debug("GOIB - INFO ====== instanciació del servei de WS @firma");
	            DSSCertificateService dssCertificateService = new DSSCertificateService();
	            logger.debug("GOIB - INFO ====== instanciació de dssCertificate");
	            DSSCertificate dssCertificate = dssCertificateService.getDSSAfirmaVerifyCertificate();
	            logger.debug("GOIB - INFO ====== indicar credencials pel webservice");
	            // Cridada al Webservice amb autenticació
	            BindingProvider prov = (BindingProvider)dssCertificate;
	            prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "usuari");
	            prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "password");
	            // FI Cridada al Webservice amb autenticació
	            
	            // Alternativa usuari i contrasenya
	            Map<String,Object> req_ctx = ((BindingProvider)dssCertificate).getRequestContext();
	            Map<String, List<String>> headers = new HashMap<String, List<String>>();
	            headers.put("Username", Collections.singletonList("usuari"));
	            headers.put("Password", Collections.singletonList("password"));
	            req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
	            // FI Alternativa usuari i contrasenya
	            
	            logger.debug("GOIB - INFO ====== cridada al webservice de verificació");
	            String resultatVerificacio=dssCertificate.verify(dssXML);

	            logger.debug("GOIB - Resultat Verificacio (static): " + resultatVerificacio);
	            
	            // Obtenim la informació sobre l'usuari autenticat del resultat del webservice
	            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	            Document document = docBuilder.parse(new InputSource(new StringReader(resultatVerificacio)));
	            	            	            
	            XPath xPath = XPathFactory.newInstance().newXPath();
	            //String xPathQuery="//*[local-name()='ReadableField'][*[local-name()='FieldIdentity']='NIFResponsable']/*[local-name()='FieldValue']/text()";
	            String xPathQuery=System.getProperty("es.caib.autenticacio.afirma.xpathdni");
	            String nif = (String)xPath.evaluate(xPathQuery, document, XPathConstants.STRING);
	            try {
	            	logger.debug("GOIB - INFO ===== info node: " + nif);
	            }catch(DOMException de) {
	            	logger.error("GOIB - DOM Exception" + de + de.getCause() + "\n"+ de.getStackTrace());
	            }
	            	            
	            
	            UserModel user;
	            try {
	                context.getEvent().detail(Details.USERNAME, nif);
	                context.getAuthenticationSession().setAuthNote(AbstractUsernameFormAuthenticator.ATTEMPTED_USERNAME, nif);
	                user = getUserIdentityToModelMapper(config).find(context, nif);
	            }
	            catch(ModelDuplicateException e) {
	                logger.modelDuplicateException(e);
	                String errorMessage = "X509 certificate authentication's failed.";
	                // TODO is calling form().setErrors enough to show errors on login screen?
	                context.challenge(createErrorResponse(context, certs[0].getSubjectDN().getName(),
	                        errorMessage, e.getMessage()));
	                context.attempted();
	                return;
	            }

	            if (invalidUser(context, user)) {
	                // TODO use specific locale to load error messages
	                String errorMessage = "X509 certificate authentication's failed.";
	                // TODO is calling form().setErrors enough to show errors on login screen?
	                context.challenge(createErrorResponse(context, certs[0].getSubjectDN().getName(),
	                        errorMessage, "Invalid user"));
	                context.attempted();
	                return;
	            }

	            if (!userEnabled(context, user)) {
	                // TODO use specific locale to load error messages
	                String errorMessage = "X509 certificate authentication's failed.";
	                // TODO is calling form().setErrors enough to show errors on login screen?
	                context.challenge(createErrorResponse(context, certs[0].getSubjectDN().getName(),
	                        errorMessage, "User is disabled"));
	                context.attempted();
	                return;
	            }
	            if (context.getRealm().isBruteForceProtected()) {
	                if (context.getProtector().isTemporarilyDisabled(context.getSession(), context.getRealm(), user)) {
	                    context.getEvent().user(user);
	                    context.getEvent().error(Errors.USER_TEMPORARILY_DISABLED);
	                    // TODO use specific locale to load error messages
	                    String errorMessage = "X509 certificate authentication's failed.";
	                    // TODO is calling form().setErrors enough to show errors on login screen?
	                    context.challenge(createErrorResponse(context, certs[0].getSubjectDN().getName(),
	                            errorMessage, "User is temporarily disabled. Contact administrator."));
	                    context.attempted();
	                    return;
	                }
	            }
	            context.setUser(user);

	            // Check whether to display the identity confirmation
	            if (!config.getConfirmationPageDisallowed()) {
	                // FIXME calling forceChallenge was the only way to display
	                // a form to let users either choose the user identity from certificate
	                // or to ignore it and proceed to a normal login screen. Attempting
	                // to call the method "challenge" results in a wrong/unexpected behavior.
	                // The question is whether calling "forceChallenge" here is ok from
	                // the design viewpoint?
	                context.forceChallenge(createSuccessResponse(context, certs[0].getSubjectDN().getName()));
	                // Do not set the flow status yet, we want to display a form to let users
	                // choose whether to accept the identity from certificate or to specify username/password explicitly
	            }
	            else {
	                // Bypass the confirmation page and log the user in
	                context.success();
	            }
	        }
	        catch(Exception e) {
	            logger.errorf("[X509ClientCertificateAuthenticator:authenticate] Exception: %s", e.getMessage());
	            context.attempted();
	        }
	    }
	    
	    /** @autor: Antoni Sànchez Magraner*/
	    /* Agafat de keycloak/services/src/main/java/org/keycloak/authentication/authenticators/directgrant/ */
	    public Response errorResponse(int status, String error, String errorDescription) {
	        OAuth2ErrorRepresentation errorRep = new OAuth2ErrorRepresentation(error, errorDescription);
	        return Response.status(status).entity(errorRep).type(MediaType.APPLICATION_JSON_TYPE).build();
	    }
	    

	    private Response createErrorResponse(AuthenticationFlowContext context,
	                                         String subjectDN,
	                                         String errorMessage,
	                                         String ... errorParameters) {

	        return createResponse(context, subjectDN, false, errorMessage, errorParameters);
	    }

	    private Response createSuccessResponse(AuthenticationFlowContext context,
	                                           String subjectDN) {
	        return createResponse(context, subjectDN, true, null, null);
	    }

	    private Response createResponse(AuthenticationFlowContext context,
	                                         String subjectDN,
	                                         boolean isUserEnabled,
	                                         String errorMessage,
	                                         Object[] errorParameters) {

	        LoginFormsProvider form = context.form();
	        if (errorMessage != null && errorMessage.trim().length() > 0) {
	            List<FormMessage> errors = new LinkedList<>();

	            errors.add(new FormMessage(errorMessage));
	            if (errorParameters != null) {

	                for (Object errorParameter : errorParameters) {
	                    if (errorParameter == null) continue;
	                    for (String part : errorParameter.toString().split("\n")) {
	                        errors.add(new FormMessage(part));
	                    }
	                }
	            }
	            form.setErrors(errors);
	        }

	        MultivaluedMap<String,String> formData = new MultivaluedHashMap<>();
	        formData.add("username", context.getUser() != null ? context.getUser().getUsername() : "unknown user");
	        formData.add("subjectDN", subjectDN);
	        formData.add("isUserEnabled", String.valueOf(isUserEnabled));

	        form.setFormData(formData);

	        return form.createX509ConfirmPage();
	    }

	    private void dumpContainerAttributes(AuthenticationFlowContext context) {

	        Enumeration<String> attributeNames = context.getHttpRequest().getAttributeNames();
	        while(attributeNames.hasMoreElements()) {
	            String a = attributeNames.nextElement();
	            logger.tracef("[X509ClientCertificateAuthenticator:dumpContainerAttributes] \"%s\"", a);
	        }
	    }

	    private boolean userEnabled(AuthenticationFlowContext context, UserModel user) {
	        if (!user.isEnabled()) {
	            context.getEvent().user(user);
	            context.getEvent().error(Errors.USER_DISABLED);
	            return false;
	        }
	        return true;
	    }

	    private boolean invalidUser(AuthenticationFlowContext context, UserModel user) {
	        if (user == null) {
	            context.getEvent().error(Errors.USER_NOT_FOUND);
	            return true;
	        }
	        return false;
	    }

	    @Override
	    public void action(AuthenticationFlowContext context) {
	        MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
	        if (formData.containsKey("cancel")) {
	            context.clearUser();
	            context.attempted();
	            return;
	        }
	        if (context.getUser() != null) {
	            //recordX509CertificateAuditDataViaContextEvent(context);
	            context.success();
	            return;
	        }
	        context.attempted();
	    }
	}
